package com.zuitt.activity;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args){

        Phonebook myPhoneBook = new Phonebook();

        Contact Contact1 = new Contact();

        Contact1.setName("John Doe");
        Contact1.setNumber("437-2310");
        Contact1.setAddress("Tanza, Cavite");

        Contact Contact2 = new Contact("Jane Doe","437-2309","Caloocan City");


        System.out.println(Contact1.getName());
        System.out.println(Contact1.getNumber());
        System.out.println(Contact1.getAddress());

        System.out.println(Contact2.getName());
        System.out.println(Contact2.getNumber());
        System.out.println(Contact2.getAddress());

        myPhoneBook.addContact(Contact1);
        myPhoneBook.addContact(Contact2);

        boolean isEmpty = myPhoneBook.contacts.size() > 0? false : true;
        if (isEmpty) {
            System.out.println("Your phonebook is empty.");
        } else {
            ArrayList<Contact> myContacts = myPhoneBook.getContacts();
            myContacts.forEach(myContact -> {
                System.out.println("---------------------------------------------------------");
                System.out.println(myContact.getName());
                System.out.println("---------------------------------------------------------");
                System.out.println(myContact.getName() + " has the following registered number: "+myContact.getNumber());
                System.out.println(myContact.getName() + " has the following registered address: "+myContact.getAddress());
            });
        }
    }
}
