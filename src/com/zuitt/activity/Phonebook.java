package com.zuitt.activity;

import java.util.ArrayList;

public class Phonebook {

    public ArrayList<Contact> contacts = new ArrayList<Contact>();
    public Phonebook(){}
    public Phonebook(ArrayList<Contact> contacts){
        this.contacts = contacts;
    }
    public void addContact(Contact newContact){
        this.contacts.add(newContact);
    }
    // getter
    public ArrayList<Contact> getContacts (){ return this.contacts; }
}
