package com.zuitt.activity;


import com.zuitt.example.Car;
import com.zuitt.example.Dog;
import com.zuitt.example.Driver;

public class Contact {

    private String name;
    private String number;
    private String address;

    public Contact(){}
    public Contact(String name, String number, String address) {
        this.name = name;
        this.number = number;
        this.address = address;
        }
        //Getter
        public String getName(){
            return this.name;
        }
        public String getNumber(){
            return this.number;
        }
        public String getAddress(){
            return this.address;
        }

        //Getter

        public void setName(String name){
            this.name = name;
        }
        public void setNumber(String number){
            this.number = number;
        }
        public void setAddress(String address){
            this.address = address;
        }

}
